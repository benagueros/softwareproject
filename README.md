Hey, this is the git page for FitLife! The revolutionary exercise tracking software that is being made by people, for people. People like Topher.

For those that don't know, FitLife is a piece of software to- Wait, why am I writing this? Everyone who will see this is one of the contributors... Am I that desperate to feel like I am contributing that I keep writing to a readme that people will probably never read? Yes. Yes I am.

Useful gitbash commands below:

git clone https://RylBeaver@bitbucket.org/benagueros/softwareproject.git

git add <filename>

git commit -m "Commit Message"

git push origin master

vim <filename>

	Opens a text editor where, unless you know what you're doing, you can never leave

